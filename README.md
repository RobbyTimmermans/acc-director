# ACC Director


This is a springboot app that reads data from a ACC udp stream. With a simple html/javascript frontend.

Can be used for single or online multiplayer races but was created with online multiplayer races in mind.

## Features
* shows car list
* focus on specific car
* shows best session laps, best personal lap, last lap time per car
* shows car location (pit, pitexit, pitentry, track)
* shows penalty message for a car
* configurable autofocus
    * configure switch interval in seconds 
    * configure start delay, start autofocus in x-seconds (can be used with race start to focus a bit longer on the front row)

## Setup for online multiplayer broadcast
* if you race in the same mulitplayer race you need 2 separate steam acc licences.
* on broadcast pc
    * install acc
    * open acc
    * connect race with broadcast password (can be requested from the race admin)
    * configure broadcast.json (https://www.assettocorsa.net/forum/index.php?threads/lets-talk-about-broadcasting-users-thread.53828/)
    * install the springboot app (on this machine or other machine)
    * connect to the correct ip

# TODO

* use logger instead of sout
* rename indexcontroller to connection controller
* config index.html to redirect to connection page
* cycle through camera's
* option to start autofocus on session start
* show session remaining time
* show track info and general BroadCastMessages
* show delta's between drivers
* option to focus on leader finish
* installer, for easy installing on windows
* option to configure messages to file for obs integration (like fastest lap op the session)
* use websockets instead of polling
* connect with multiple clients
* support for multiple drivers
* add rest endpoints
* add some integration tests

