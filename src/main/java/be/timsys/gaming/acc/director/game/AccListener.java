package be.timsys.gaming.acc.director.game;

import be.timsys.gaming.acc.director.config.Config;
import be.timsys.gaming.acc.director.game.model.AccMessage;
import be.timsys.gaming.acc.director.game.model.response.Connection;
import be.timsys.gaming.acc.director.game.model.response.RealTimeCarUpdate;
import be.timsys.gaming.acc.director.game.model.response.RealTimeUpdate;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.time.OffsetDateTime;

@Service
@RequiredArgsConstructor
public class AccListener {

    private AccClient accClient;
    private SocketReader socketReader;

    private final Config config;
    private final AccMessageFactory accMessageFactory;
    private final ApplicationEventPublisher publisher;
    private Thread thread;
    private File dumpFile;
    private FileOutputStream fos;

    void startListening(InetAddress ip, int port) throws Exception {
        accClient = new AccClient();
        accClient.init(ip, port);

        socketReader = new SocketReader();

        thread = new Thread(socketReader, "SocketReader");
        thread.start();

        accClient.requestConnection();
        initFile();
    }

    void stop(Connection connection) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.shutdown(connection);
            accClient = null;
            thread.stop();
        }

        if (fos != null) {
            fos.close();
        }
    }

    void focus(Connection connection, short carIndex) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.requestFocus(connection, carIndex);
        }
    }

    void hudPage(Connection connection, String hudPage) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.requestHud(connection, hudPage);
        }
    }

    void requestEntryList(Connection connection) throws IOException {
        if (accClient != null && accClient.isConnected()) {
            accClient.requestEntryList(connection);
        }
    }

    private void initFile() {
        dumpFile = new File(config.getSavedSessionsFolder().toFile(), String.format("dump-%ts.acc", OffsetDateTime.now()));
        try {
            fos = new FileOutputStream(dumpFile, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private class SocketReader implements Runnable {
        @Override
        public void run() {
            if (accClient != null) {
                try {
                    byte[] read = accClient.read();
                    while (read != null) {
                        processMessage(read);
                        read = accClient.read();
                        fos.write(read);
                    }
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        }

        private void processMessage(byte[] read) throws IOException {
            AccMessage accMessage = accMessageFactory.create(read);

            if (accMessage instanceof Connection) {
                accClient.requestEntryList((Connection) accMessage);
                accClient.requestTrackData((Connection) accMessage);
            }

            if (accMessage != null) {
                if (!(accMessage instanceof RealTimeCarUpdate) && !(accMessage instanceof RealTimeUpdate)) {
                    System.out.println(accMessage);
                }
                publisher.publishEvent(accMessage);
            }
        }
    }
}
