package be.timsys.gaming.acc.director.director.model;

import lombok.Data;

@Data
public class Car {

    private Short index;
    private Integer number;
    private String driverShortName;
    private String driverLongName;
    private boolean hasFocus;
    private Short sessionPosition;
    private CarLocation carLocation;
    private String status;
}
