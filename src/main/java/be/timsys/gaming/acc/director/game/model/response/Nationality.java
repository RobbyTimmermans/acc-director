package be.timsys.gaming.acc.director.game.model.response;

import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

@Getter
public enum Nationality {

    Any((short) 0, "Any"),
    Italy((short) 1, "Italy"),
    Germany((short) 2, "Germany"),
    France((short) 3, "France"),
    Spain((short) 4, "Spain"),
    GreatBritain((short) 5, "GreatBritain"),
    Hungary((short) 6, "Hungary"),
    Belgium((short) 7, "Belgium"),
    Switzerland((short) 8, "Switzerland"),
    Austria((short) 9, "Austria"),
    Russia((short) 10, "Russia"),
    Thailand((short) 11, "Thailand"),
    Netherlands((short) 12, "Netherlands"),
    Poland((short) 13, "Poland"),
    Argentina((short) 14, "Argentina"),
    Monaco((short) 15, "Monaco"),
    Ireland((short) 16, "Ireland"),
    Brazil((short) 17, "Brazil"),
    SouthAfrica((short) 18, "SouthAfrica"),
    PuertoRico((short) 19, "PuertoRico"),
    Slovakia((short) 20, "Slovakia"),
    Oman((short) 21, "Oman"),
    Greece((short) 22, "Greece"),
    SaudiArabia((short) 23, "SaudiArabia"),
    Norway((short) 24, "Norway"),
    Turkey((short) 25, "Turkey"),
    SouthKorea((short) 26, "SouthKorea"),
    Lebanon((short) 27, "Lebanon"),
    Armenia((short) 28, "Armenia"),
    Mexico((short) 29, "Mexico"),
    Sweden((short) 30, "Sweden"),
    Finland((short) 31, "Finland"),
    Denmark((short) 32, "Denmark"),
    Croatia((short) 33, "Croatia"),
    Canada((short) 34, "Canada"),
    China((short) 35, "China"),
    Portugal((short) 36, "Portugal"),
    Singapore((short) 37, "Singapore"),
    Indonesia((short) 38, "Indonesia"),
    USA((short) 39, "USA"),
    NewZealand((short) 40, "NewZealand"),
    Australia((short) 41, "Australia"),
    SanMarino((short) 42, "SanMarino"),
    UAE((short) 43, "UAE"),
    Luxembourg((short) 44, "Luxembourg"),
    Kuwait((short) 45, "Kuwait"),
    HongKong((short) 46, "HongKong"),
    Colombia((short) 47, "Colombia"),
    Japan((short) 48, "Japan"),
    Andorra((short) 49, "Andorra"),
    Azerbaijan((short) 50, "Azerbaijan"),
    Bulgaria((short) 51, "Bulgaria"),
    Cuba((short) 52, "Cuba"),
    CzechRepublic((short) 53, "CzechRepublic"),
    Estonia((short) 54, "Estonia"),
    Georgia((short) 55, "Georgia"),
    India((short) 56, "India"),
    Israel((short) 57, "Israel"),
    Jamaica((short) 58, "Jamaica"),
    Latvia((short) 59, "Latvia"),
    Lithuania((short) 60, "Lithuania"),
    Macau((short) 61, "Macau"),
    Malaysia((short) 62, "Malaysia"),
    Nepal((short) 63, "Nepal"),
    NewCaledonia((short) 64, "NewCaledonia"),
    Nigeria((short) 65, "Nigeria"),
    NorthernIreland((short) 66, "NorthernIreland"),
    PapuaNewGuinea((short) 67, "PapuaNewGuinea"),
    Philippines((short) 68, "Philippines"),
    Qatar((short) 69, "Qatar"),
    Romania((short) 70, "Romania"),
    Scotland((short) 71, "Scotland"),
    Serbia((short) 72, "Serbia"),
    Slovenia((short) 73, "Slovenia"),
    Taiwan((short) 74, "Taiwan"),
    Ukraine((short) 75, "Ukraine"),
    Venezuela((short) 76, "Venezuela"),
    Wales((short) 77, "Wales");

    Nationality(short id, String label) {
        this.id = id;
        this.label = label;
    }

    private short id;
    private String label;

    public static Optional<Nationality> fromId(short id) {
        return Stream.of(values()).filter(cl -> cl.id == id).findFirst();
    }
}
