package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.SessionType;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CarSessionReport {

    private SessionType sessionType;
    private List<Event> carEvents;
    private FastestLap fastestLap;
    private short numberOfPitStops;
    private short sessionLastPosition;
    private int sessionTotalTimeMs;
    private TheoreticalBest theoreticalBest;
    private MaxSpeed maxSpeed;
}
