package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.Lap;
import lombok.Data;

import java.util.List;

@Data
public class FullReport {

    private String track;
    private String date;
    private Lap fastestLap;
    private String fastestLapCarAndDriver;

    private List<CarReport> carReports;

    private List<SessionResult> sessionReports;
}
