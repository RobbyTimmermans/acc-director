package be.timsys.gaming.acc.director.game.model.response;

import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;

import static be.timsys.gaming.acc.director.game.model.AccMessage.readString;

@Getter
@ToString
public class Driver {

    private short driverIndex;
    private String firstName;
    private String lastName;
    private String shortName;
    private byte category;
    private short nationality;

    Driver(short driverIndex, LittleEndianDataInputStream lil) throws IOException {
        this.driverIndex = driverIndex;
        this.firstName = readString(lil);
        this.lastName = readString(lil);
        this.shortName = readString(lil);
        this.category = lil.readByte();
        this.nationality = lil.readShort();
    }

    public String getDriverFullRepresentation() {
        return String.format("%s %s (%s - %s)", firstName, lastName, shortName, Nationality.fromId(nationality).map(Nationality::getLabel).orElse(""));
    }

    public String getDriverFullName() {
        return String.format("%s %s", firstName, lastName);
    }
}
