package be.timsys.gaming.acc.director.session;

import be.timsys.gaming.acc.director.config.Config;
import be.timsys.gaming.acc.director.game.Acc;
import be.timsys.gaming.acc.director.game.AccMessageFactory;
import be.timsys.gaming.acc.director.game.model.response.RealTimeCarUpdate;
import be.timsys.gaming.acc.director.game.model.response.RealTimeUpdate;
import be.timsys.gaming.acc.director.game.model.response.SessionType;
import be.timsys.gaming.acc.director.game.model.response.TrackData;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SessionManager {

    private final Config config;
    private final AccMessageFactory accMessageFactory;
    private final Acc acc;
    private final ApplicationEventPublisher publisher;

    public List<Session> getSavedSessions() throws IOException {
        var files = Files.list(config.getSavedSessionsFolder());

        return files
                .filter(this::isSession)
                .map(this::toSession)
                .sorted(Collections.reverseOrder(Comparator.comparing(Session::getTimeStamp)))
                .collect(Collectors.toList());
    }

    void replaySession(String sessionPath) throws IOException {
        acc.disconnect();

        byte[] read = new byte[1024];
        try (FileInputStream fileInputStream = new FileInputStream(sessionPath)) {
            while (fileInputStream.available() > 0) {
                fileInputStream.read(read);
                var accMessage = accMessageFactory.create(read);
                if (accMessage != null) {
                    if (!(accMessage instanceof RealTimeCarUpdate) && !(accMessage instanceof RealTimeUpdate)) {
                        System.out.println(accMessage);
                    }
                    publisher.publishEvent(accMessage);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Session toSession(Path f) {
        try {
            var attributes = Files.readAttributes(f, BasicFileAttributes.class);

            var session = Session.builder()
                    .path(f.toAbsolutePath().toString())
                    .name(f.getFileName().toString())
                    .timeStamp(OffsetDateTime.ofInstant(attributes.creationTime().toInstant(), ZoneId.systemDefault()))
                    .build();

            return enrichWithDumpData(session, f);
        } catch (IOException iox) {
            throw new RuntimeException(iox);
        }
    }

    private Session enrichWithDumpData(Session session, Path path) {
        byte[] read = new byte[1024];
        try (FileInputStream fileInputStream = new FileInputStream(path.toFile())) {
            while (fileInputStream.available() > 0) {
                fileInputStream.read(read);
                var accMessage = accMessageFactory.create(read);
                if (accMessage instanceof TrackData) {
                    session.setTrack(((TrackData) accMessage).getTrackName());
                }
                if (accMessage instanceof RealTimeUpdate) {
                    session.addSessionType(SessionType.fromId(((RealTimeUpdate) accMessage).getSessionType()).orElse(null));
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return session;
    }

    @SneakyThrows
    private boolean isSession(Path path) {
        var attributes = Files.readAttributes(path, BasicFileAttributes.class);
        return !attributes.isDirectory() && path.getFileName().toString().endsWith(".acc");
    }
}
