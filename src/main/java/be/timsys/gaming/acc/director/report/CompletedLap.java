package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.Lap;
import be.timsys.gaming.acc.director.game.model.response.SessionType;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CompletedLap implements Comparable<CompletedLap>{

    private final SessionType sessionType;
    private final short lapNumber;
    private final Lap lap;
    private final short position;

    @Override
    public int compareTo(CompletedLap o) {
        return this.getLap().compareTo(o.getLap());
    }
}
