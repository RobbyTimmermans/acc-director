package be.timsys.gaming.acc.director.game.model.response;

import java.util.Arrays;
import java.util.Optional;

public enum SessionType {
    PRACTICE((short) 0),
    QUALIFYING((short) 4),
    SUPERPOLE((short) 9),
    RACE((short) 10),
    HOTLAP((short) 11),
    HOTSTINT((short) 12),
    HOTLAPSUPERPOLE((short) 13),
    REPLAY((short) 14);

    private short id;

    SessionType(short id) {
        this.id = id;
    }

    public static Optional<SessionType> fromId(Short id) {
        return Arrays.stream(values()).filter(s -> s.id == id).findFirst();
    }
}
