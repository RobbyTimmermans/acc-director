package be.timsys.gaming.acc.director.director.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public class TrackInfo {

    private final String trackName;
    private final Map<String, List<String>> cameraSets;

}
