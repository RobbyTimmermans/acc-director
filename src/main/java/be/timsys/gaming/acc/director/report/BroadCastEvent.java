package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.director.model.BroadCastMessageType;
import be.timsys.gaming.acc.director.game.model.BroadCastMessage;
import be.timsys.gaming.acc.director.game.model.response.SessionType;

public class BroadCastEvent extends Event {

    private BroadCastMessage broadCastMessage;

    BroadCastEvent(SessionType sessionType, BroadCastMessage broadCastMessage) {
        super(sessionType);
        this.broadCastMessage = broadCastMessage;
    }

    @Override
    public String getDescription() {
        return String.format("%s - %s", getType(), broadCastMessage.getMessage());
    }

    private String getType() {
        return BroadCastMessageType.fromId(broadCastMessage.getType()).map(BroadCastMessageType::getLabel).orElse("");
    }
}
