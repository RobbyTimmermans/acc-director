package be.timsys.gaming.acc.director.game.model.response;

import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Lap implements Comparable<Lap> {

    private Integer laptimeInMs;
    private Duration laptime;
    private short carIndex;
    private short driverIndex;
    private List<Integer> splits;
    private boolean invalid;
    private boolean isValidForBest;
    private boolean isOutlap;
    private boolean isInlap;

    public Lap(LittleEndianDataInputStream lil) throws IOException {
        this.laptimeInMs = lil.readInt();
        this.laptime = Duration.ofMillis(laptimeInMs);
        this.carIndex = lil.readShort();
        this.driverIndex = lil.readShort();
        this.splits = getSplits(lil);
        this.invalid = lil.readBoolean();
        this.isValidForBest = lil.readBoolean();
        this.isOutlap = lil.readBoolean();
        this.isInlap = lil.readBoolean();
    }

    private List<Integer> getSplits(LittleEndianDataInputStream lil) throws IOException {
        short splitCount = lil.readByte();

        List<Integer> splits = new ArrayList<>(splitCount);

        for (short i = 0; i < splitCount; i++) {
            splits.add(lil.readInt());
        }
        return splits;
    }

    @Override
    public String toString() {
        return "Lap{" +
                "laptimeInMs=" + laptimeInMs +
                ", laptime=" + DurationFormatUtils.formatDurationHMS(laptimeInMs) +
                ", carIndex=" + carIndex +
                ", driverIndex=" + driverIndex +
                ", splits=" + splits +
                ", invalid=" + invalid +
                ", isValidForBest=" + isValidForBest +
                ", isOutlap=" + isOutlap +
                ", isInlap=" + isInlap +
                '}';
    }

    @Override
    public int compareTo(Lap o) {
        return this.getLaptimeInMs().compareTo(o.getLaptimeInMs());
    }

    public String getFormattedTime() {
        if (Integer.MAX_VALUE == laptimeInMs) {
            return "0:00:00";
        } else {
            return DurationFormatUtils.formatDuration(laptimeInMs, "mm:ss.SSS", true);
        }
    }

    public String getFormattedSplits(int splitIndex) {
        if (splitIndex > this.splits.size()) {
            return "00:00.000";
        } else {
            int split = splits.get(splitIndex);
            if (Integer.MAX_VALUE == split) {
                return "00:00.000";
            } else {
                return DurationFormatUtils.formatDuration(split, "mm:ss.SSS", true);
            }
        }
    }
}


