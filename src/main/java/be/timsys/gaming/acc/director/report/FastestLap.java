package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.Lap;
import be.timsys.gaming.acc.director.game.model.response.SessionType;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class FastestLap {

    private final Short lapNumber;
    private final Lap lap;
    private final SessionType sessionType;
    private final boolean sessionBest;
}
