package be.timsys.gaming.acc.director.report;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class MaxSpeed {

    private final short kmh;
    private final short lap;
}
