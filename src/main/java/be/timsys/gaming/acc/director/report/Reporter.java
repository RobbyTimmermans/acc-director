package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.director.model.BroadCastMessageType;
import be.timsys.gaming.acc.director.game.Acc;
import be.timsys.gaming.acc.director.game.model.BroadCastMessage;
import be.timsys.gaming.acc.director.game.model.response.*;
import be.timsys.gaming.acc.director.index.DisconnectedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class Reporter {

    private Map<Short, List<Event>> carEvents = new HashMap<>();
    private Map<Short, Lap> carLastLaps = new HashMap<>();
    private Map<Short, List<CompletedLap>> carLaps = new HashMap<>();
    private Map<Short, Map<SessionType, MaxSpeed>> carMaxSpeeds = new HashMap<>();
    private Map<Short, BroadCastMessage> carLastBroadCastMessage = new HashMap<>();

    private final Acc acc;

    @EventListener
    public void onBroadCastMessage(BroadCastMessage broadCastMessage) {
        var carId = (short) broadCastMessage.getCarId();
        if (broadCastMessage.getType() != BroadCastMessageType.LAPCOMPLETED.getId()
                && broadCastMessage.getType() != BroadCastMessageType.BESTPERSONALLAP.getId()
                && broadCastMessage.getType() != BroadCastMessageType.BESTSESSIONLAP.getId()) {
            if (!carLastBroadCastMessage.containsKey(carId) || !carLastBroadCastMessage.get(carId).equals(broadCastMessage)) {
                acc.getCurrentSession().ifPresent(s -> addEvent(carId, new BroadCastEvent(s, broadCastMessage)));
            }
        }
        carLastBroadCastMessage.put(carId, broadCastMessage);
    }

    @EventListener
    public void onRealtimeCarUpdate(RealTimeCarUpdate update) {
        var carId = update.getCarIndex();

        acc.getCurrentSession().ifPresent(t -> {
            if (!carMaxSpeeds.containsKey(carId)) {
                var speeds = new HashMap<SessionType, MaxSpeed>();
                speeds.put(t, new MaxSpeed(update.getKmh(), update.getLaps()));
                carMaxSpeeds.put(carId, speeds);
            } else {
                var carMaxSpeedsPerSession = carMaxSpeeds.get(carId);
                if (!carMaxSpeedsPerSession.containsKey(t)) {
                    carMaxSpeedsPerSession.put(t, new MaxSpeed(update.getKmh(), update.getLaps()));
                } else {
                    if (update.getKmh() > carMaxSpeedsPerSession.get(t).getKmh()) {
                        carMaxSpeedsPerSession.put(t, new MaxSpeed(update.getKmh(), update.getLaps()));
                    }
                }
            }

            if (!carLastLaps.containsKey(carId) || !carLastLaps.get(carId).getLaptimeInMs().equals(update.getLastLap().getLaptimeInMs())) {
                var entryList = acc.getEntryList();
                var car = entryList.stream().filter(e -> e.getCarIndex() == carId).findFirst().orElse(null);
                Driver currentDriver = null;
                if (car != null && car.getDrivers() != null) {
                    currentDriver = car.getDrivers().stream().filter(d -> d.getDriverIndex() == update.getDriverIndex()).findFirst().orElse(null);
                }
                addEvent(carId, new LapCompletedEvent(t, update.getLaps(), update.getSessionPosition(), update.getLastLap(), currentDriver));

                if (!carLaps.containsKey(carId)) {
                    carLaps.put(carId, new ArrayList<>());
                }

                carLaps.get(carId).add(new CompletedLap(t, update.getLaps(), update.getLastLap(), update.getSessionPosition()));
            }
        });
        carLastLaps.put(carId, update.getLastLap());
    }

    private void addEvent(short carId, Event event) {
        if (!carEvents.containsKey(carId)) {
            carEvents.put(carId, new ArrayList<>());
        }
        carEvents.get(carId).add(event);
    }

    CarReport getCarReport(short carId) {
        return acc.getEntryList().stream()
                .filter(e -> e.getCarIndex() == carId)
                .findFirst()
                .map(this::createReport).orElse(null);
    }

    public FullReport getFullReport() {
        var fullReport = new FullReport();

        var reports = acc.getEntryList()
                .stream()
                .map(e -> getCarReport(e.getCarIndex()))
                .sorted(Comparator.comparing(CarReport::getCarNumber))
                .collect(toList());

        fullReport.setCarReports(reports);
        fullReport.setTrack(acc.getTrackData().map(TrackData::getTrackName).orElse(""));
        fullReport.setDate(LocalDate.now().format(DateTimeFormatter.ISO_DATE));

        var sessionResults = carLaps.values().stream().flatMap(Collection::stream)
                .map(CompletedLap::getSessionType)
                .distinct()
                .map(sessionType -> new SessionResult(sessionType, Collections.emptyList()))
                .collect(toList());

        fullReport.setSessionReports(sessionResults);
        return fullReport;
    }

    private CarReport createReport(EntryListCar e) {
        var report = new CarReport();

        report.setTrack(acc.getTrackData().map(TrackData::getTrackName).orElse(""));

        report.setCarNumber(String.valueOf(e.getRaceNumber()));
        report.setTeamName(e.getTeamName());
        report.setCarType(e.getCarModelType());
        if (e.getDrivers() != null) {
            report.setDriverNames(e.getDrivers().stream().map(Driver::getDriverFullRepresentation).collect(toList()));
        }

        var carEvents = this.carEvents.getOrDefault(e.getCarIndex(), Collections.emptyList());
        var sessions = carEvents.stream().map(Event::getSessionType).distinct().collect(toList());
        report.setSessions(sessions);

        report.setSessionReports(sessions.stream()
                .map(s -> createCarSessionReport(e, carEvents, s))
                .collect(toList()));

        return report;
    }

    private CarSessionReport createCarSessionReport(EntryListCar e, List<Event> carEvents, SessionType s) {
        var sessionEvents = getSessionEvents(s, carEvents);
        var fastestLap = getFastestLap(s, carLaps.get(e.getCarIndex()));
        var numberOfPitStops = (short) getNumberOfPitStops(s, carLaps.get(e.getCarIndex()));
        var lastPosition = getLastPosition(s, carLaps.get(e.getCarIndex()));
        var totalTime = getTotalTime(s, carLaps.get(e.getCarIndex()));
        return CarSessionReport.builder()
                .sessionType(s)
                .carEvents(sessionEvents)
                .fastestLap(fastestLap)
                .numberOfPitStops(numberOfPitStops)
                .sessionLastPosition(lastPosition)
                .sessionTotalTimeMs(totalTime)
                .theoreticalBest(getTheoreticalBest(s, carLaps.get(e.getCarIndex())))
                .maxSpeed(getMaxSpeed(e.getCarIndex(), s))
                .build();
    }

    private MaxSpeed getMaxSpeed(Short carId, SessionType sessionType) {
        if (carMaxSpeeds.containsKey(carId)) {
            if (carMaxSpeeds.get(carId).containsKey(sessionType)) {
                return carMaxSpeeds.get(carId).get(sessionType);
            }
        }
        return null;
    }

    private TheoreticalBest getTheoreticalBest(SessionType s, List<CompletedLap> completedLaps) {
        TheoreticalBest theo = new TheoreticalBest();
        var sessionCompletedLaps = completedLaps.stream().filter(c -> c.getSessionType().equals(s)).collect(Collectors.toList());

        int numberOfSplits = sessionCompletedLaps.stream().filter(c -> c.getLap().getSplits().size() > 0).findFirst().map(c -> c.getLap().getSplits().size()).orElse(0);
        for (int splitIndex = 0; splitIndex < numberOfSplits; splitIndex++) {
            theo.addSplit(findBestSplit(sessionCompletedLaps, splitIndex));
        }
        return theo;
    }

    private Integer findBestSplit(List<CompletedLap> completedLaps, int i) {
        return completedLaps.stream().filter(c -> c.getLap().getSplits().size() > i)
                .filter(c -> c.getLap().isValidForBest())
                .filter(c -> c.getLap().getLaptimeInMs() > 0)
                .map(c -> c.getLap().getSplits().get(i))
                .filter(splits -> splits < Integer.MAX_VALUE)
                .min(Integer::compareTo).orElse(0);
    }

    private Integer getTotalTime(SessionType s, List<CompletedLap> completedLaps) {
        return completedLaps.stream().filter(c -> c.getSessionType().equals(s))
                .mapToInt(c -> c.getLap().getLaptimeInMs())
                .sum();
    }

    private short getLastPosition(SessionType s, List<CompletedLap> completedLaps) {
        return completedLaps.stream().filter(c -> c.getSessionType().equals(s))
                .reduce((first, second) -> second)
                .map(CompletedLap::getPosition)
                .orElse((short) 0);
    }

    private long getNumberOfPitStops(SessionType s, List<CompletedLap> completedLaps) {
        return completedLaps.stream().filter(c -> c.getSessionType().equals(s))
                .filter(c -> c.getLap().isInlap()).count();
    }

    private FastestLap getFastestLap(SessionType s, List<CompletedLap> carLaps) {
        return carLaps.stream().filter(e -> e.getSessionType().equals(s))
                .filter(e -> !e.getLap().isInvalid())
                .min(CompletedLap::compareTo).map(lap -> new FastestLap(lap.getLapNumber(), lap.getLap(), lap.getSessionType(), isSessionBest(s, lap))).orElse(null);
    }

    private boolean isSessionBest(SessionType s, CompletedLap lap) {
        return carLaps.values().stream().flatMap(Collection::stream)
                .filter(l -> l.getSessionType().equals(s))
                .filter(l -> !l.getLap().isInvalid())
                .filter(l -> l.getLap().compareTo(lap.getLap()) < 0)
                .findFirst().isEmpty();
    }

    private List<Event> getSessionEvents(SessionType s, List<Event> carEvents) {
        return carEvents.stream().filter(e -> e.getSessionType().equals(s)).collect(toList());
    }

    @EventListener
    public void clearReportData(DisconnectedEvent disconnectedEvent) {
        carEvents = new HashMap<>();
        carLastLaps = new HashMap<>();
        carLaps = new HashMap<>();
        carLastBroadCastMessage = new HashMap<>();
    }
}
