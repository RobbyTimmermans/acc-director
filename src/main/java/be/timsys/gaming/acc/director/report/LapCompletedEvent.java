package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.Driver;
import be.timsys.gaming.acc.director.game.model.response.Lap;
import be.timsys.gaming.acc.director.game.model.response.SessionType;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LapCompletedEvent extends Event {

    private final short lapNumber;
    private final short position;
    private final Lap lap;
    private final Driver currentDriver;

    LapCompletedEvent(SessionType sessionType, short lapNumber, short position, Lap lap, Driver currentDriver) {
        super(sessionType);
        this.lapNumber = lapNumber;
        this.position = position;
        this.lap = lap;
        this.currentDriver = currentDriver;
    }

    @Override
    public String getDescription() {
        return String.format("Lap %03d - P%02d - %s - %s - %s %s%s %s", lapNumber, position, currentDriver != null ? currentDriver.getShortName() : "???", getSplits(lap), lap.getFormattedTime(), lap.isInlap() ? "[PITIN]" : "", lap.isOutlap() ? "[PITOUT]" : "", lap.isInvalid() ? "[INVALID]" : "");
    }

    private String getSplits(Lap lap) {
        int splitsCount = lap.getSplits().size();
        return IntStream.range(0, splitsCount).mapToObj(lap::getFormattedSplits).collect(Collectors.joining(" - "));
    }
}
