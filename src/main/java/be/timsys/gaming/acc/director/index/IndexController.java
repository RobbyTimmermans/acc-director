package be.timsys.gaming.acc.director.index;

import be.timsys.gaming.acc.director.game.Acc;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/index")
@RequiredArgsConstructor
public class IndexController {

    private final Acc acc;

    @GetMapping
    public String show() {
        return "index";
    }

    @PostMapping("/connect")
    public String connect(@RequestParam("ip") String ip) throws Exception {
        acc.connect(ip, 9000);
        return "redirect:/director/";
    }

    @GetMapping("/disconnect")
    public String disconnect() throws Exception {
        acc.disconnect();
        return "index";
    }
}
