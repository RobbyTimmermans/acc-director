package be.timsys.gaming.acc.director.director;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Controller
@RequestMapping("/director")
@RequiredArgsConstructor
public class DirectorController {

    private final DirectorFacade directorFacade;

    @GetMapping
    public ModelAndView showDirectorConsole() {
        return new ModelAndView("director");
    }

    @GetMapping("/cars")
    public ModelAndView getCars() {
        var modelAndView = new ModelAndView("cars");
        modelAndView.addObject("cars", directorFacade.getCars());
        return modelAndView;
    }

    @GetMapping("/sessionInfo")
    public ModelAndView getSessionInfo() {
        var modelAndView = new ModelAndView("session-info");
        modelAndView.addObject("sessionInfo", directorFacade.getSessionInfo());
        return modelAndView;
    }

    @GetMapping("/cars/{carId}/focus")
    public ModelAndView focus(@PathVariable("carId") Short carId) throws IOException {
        var modelAndView = new ModelAndView("cars");

        directorFacade.focus(carId);

        modelAndView.addObject("cars", directorFacade.getCars());
        return modelAndView;
    }

    @PutMapping("/cars/auto-focus")
    @ResponseStatus(value = HttpStatus.OK)
    public void startAutoFocus(@RequestParam(value = "intervalInSeconds", required = false, defaultValue = "10") Integer intervalInSeconds,
                               @RequestParam(value = "delayInSeconds", required = false, defaultValue = "0") Integer delayInSeconds) {
        directorFacade.startAutoFocus(intervalInSeconds, delayInSeconds);
    }

    @DeleteMapping("/cars/auto-focus")
    @ResponseStatus(value = HttpStatus.OK)
    public void stopAutoFocus() {
        directorFacade.stopAutoFocus();
    }
}
