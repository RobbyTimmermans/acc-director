package be.timsys.gaming.acc.director.game;

import be.timsys.gaming.acc.director.game.model.*;
import be.timsys.gaming.acc.director.game.model.response.*;
import com.google.common.io.LittleEndianDataInputStream;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Component
public class AccMessageFactory {

    public AccMessage create(byte[] raw) throws IOException {
        LittleEndianDataInputStream lil = new LittleEndianDataInputStream(new ByteArrayInputStream(raw));

        byte messageType = lil.readByte();

        switch (messageType) {
            case 1: {
                return new Connection(lil, raw);
            }
            case 2: {
                return new RealTimeUpdate(lil, raw);
            }
            case 3: {
                return new RealTimeCarUpdate(lil, raw);
            }
            case 4: {
                return new EntryList(lil, raw);
            }
            case 5: {
                return new TrackData(lil, raw);
            }
            case 6: {
                return new EntryListCar(lil, raw);
            }
            case 7: {
                return new BroadCastMessage(lil, raw);
            }
            default:
                return null;
        }

    }
}
