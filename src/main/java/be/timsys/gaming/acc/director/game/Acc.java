package be.timsys.gaming.acc.director.game;

import be.timsys.gaming.acc.director.game.model.BroadCastMessage;
import be.timsys.gaming.acc.director.game.model.EntryListCarStats;
import be.timsys.gaming.acc.director.game.model.response.*;
import be.timsys.gaming.acc.director.index.DisconnectedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

@Service
@RequiredArgsConstructor
public class Acc {

    private final AccListener accListener;
    private final ApplicationEventPublisher publisher;

    private Connection connection;
    private EntryList entryList;
    private Map<Short, EntryListCar> carMap = new HashMap<>();
    private Map<Short, EntryListCarStats> carStatsMap = new HashMap<>();

    private EntryListCar currentFocusedCar = null;
    private TrackData trackData;
    private short currentSession = -1;

    public void connect(String ip, int port) throws Exception {
        accListener.startListening(InetAddress.getByName(ip), port);
    }

    public void disconnect() throws IOException {
        publisher.publishEvent(new DisconnectedEvent());

        accListener.stop(connection);
        carMap.clear();
    }

    public void focus(short carIndex) throws IOException {
        currentFocusedCar = carMap.get(carIndex);
        accListener.focus(connection, carIndex);
    }

    public void hudPage(String hudPage) throws IOException {
        accListener.hudPage(connection, hudPage);
    }

    void requestEntryList() throws IOException {
        accListener.requestEntryList(connection);
    }

    public Optional<EntryListCar> getCurrentFocusedCar() {
        return Optional.ofNullable(currentFocusedCar);
    }

    @EventListener
    public void onConnection(Connection connection) {
        this.connection = connection;
    }

    @EventListener
    public void onEntryList(EntryList entryList) {
        this.entryList = entryList;
        if (entryList != null) {
            this.carMap = new HashMap<>();
            entryList.getCarList().forEach(c -> carMap.put(c.getCarIndex(), c));
        }
    }

    @EventListener
    public void onEntryListCar(EntryListCar entryListCar) {
        carMap.put(entryListCar.getCarIndex(), entryListCar);
    }

    @EventListener
    public void onTrackData(TrackData trackData) {
        this.trackData = trackData;
    }

    @EventListener
    public void onBroadCastMessage(BroadCastMessage broadCastMessage) {
        if (entryList != null) {
            if (carStatsMap.containsKey((short) broadCastMessage.getCarId())) {
                carStatsMap.get((short) broadCastMessage.getCarId()).setLastMessage(broadCastMessage);
            }
        }
    }

    @EventListener
    public void onRealtimeCarUpdate(RealTimeCarUpdate update) throws IOException {
        if (entryList != null) {
            if (!carMap.containsKey(update.getCarIndex())) {
                System.out.println("Update for unknown car, requesting entry list");
                requestEntryList();
            } else {
                var stats = carStatsMap.get(update.getCarIndex());
                if (stats == null) {
                    stats = new EntryListCarStats();
                }
                stats.setSessionPosition(update.getSessionPosition());
                stats.setCarLocation(update.getCarLocation());
                carStatsMap.put(update.getCarIndex(), stats);
            }
        }
    }

    @EventListener
    public void onRealTimeUpdate(RealTimeUpdate realTimeUpdate) {
        this.currentSession = realTimeUpdate.getSessionType();
    }

    public Collection<EntryListCar> getEntryList() {
        return carMap.values();
    }


    public Map<Short, EntryListCarStats> getCarStatsMap() {
        return carStatsMap;
    }

    public Optional<TrackData> getTrackData() {
        return Optional.ofNullable(trackData);
    }

    public Optional<SessionType> getCurrentSession() {
        return SessionType.fromId(this.currentSession);
    }
}
