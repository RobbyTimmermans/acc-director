package be.timsys.gaming.acc.director.game.model;

import com.google.common.io.LittleEndianDataInputStream;
import lombok.Data;

import java.io.IOException;
import java.util.Objects;

@Data
public class BroadCastMessage extends AccMessage {
    private byte type;
    private String message;
    private int timeMs;
    private int carId;

    public BroadCastMessage(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
    }

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        type = lil.readByte();
        message = readString(lil);
        timeMs = lil.readInt();
        carId = lil.readInt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BroadCastMessage that = (BroadCastMessage) o;
        return type == that.type &&
                carId == that.carId &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, message, carId);
    }
}
