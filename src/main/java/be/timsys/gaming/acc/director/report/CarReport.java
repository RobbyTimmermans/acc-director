package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.CarType;
import be.timsys.gaming.acc.director.game.model.response.SessionType;
import lombok.Data;

import java.util.List;

@Data
public class CarReport {

    private String track;
    private List<String> driverNames;
    private String teamName;
    private CarType carType;
    private String carNumber;

    private List<SessionType> sessions;
    private List<CarSessionReport> sessionReports;

    public String getDriverNamesAsString() {
        return String.join(", ", driverNames);
    }
    public String getCarDescription() {
        return String.format("%s (%s)",this.carType.getDescription(), carType.getYear());
    }
}
