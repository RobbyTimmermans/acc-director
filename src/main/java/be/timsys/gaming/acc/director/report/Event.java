package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.SessionType;

import java.time.LocalDateTime;

public abstract class Event {

    private LocalDateTime time;
    private SessionType sessionType;

    Event(SessionType sessionType) {
        time = LocalDateTime.now();
        this.sessionType = sessionType;
    }

    public String getTime() {
        return String.format("%tT", this.time);
    }

    public abstract String getDescription();

    public SessionType getSessionType() {
        return this.sessionType;
    }
}
