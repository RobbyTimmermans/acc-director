package be.timsys.gaming.acc.director.director;

import be.timsys.gaming.acc.director.director.model.BroadCastMessageType;
import be.timsys.gaming.acc.director.director.model.Car;
import be.timsys.gaming.acc.director.director.model.CarLocation;
import be.timsys.gaming.acc.director.director.model.TrackInfo;
import be.timsys.gaming.acc.director.game.Acc;
import be.timsys.gaming.acc.director.game.model.BroadCastMessage;
import be.timsys.gaming.acc.director.game.model.response.Driver;
import be.timsys.gaming.acc.director.game.model.response.EntryListCar;
import be.timsys.gaming.acc.director.game.model.response.TrackData;
import be.timsys.gaming.acc.director.index.DisconnectedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DirectorFacade {

    private final Acc acc;
    private final AutoFocusser autoFocusser;
    private Timer autoFocusTimer;

    List<Car> getCars() {
        var entryList = acc.getEntryList();
        return entryList.stream().map(this::car).sorted(Comparator.comparing(Car::getSessionPosition)).collect(Collectors.toList());
    }

    void startAutoFocus(int intervalInSeconds, Integer delayInSeconds) {
        System.out.println("Start startAutoFocus with interval " + intervalInSeconds);

        autoFocusTimer = new Timer("AutoFocusser");
        autoFocusTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    focus(autoFocusser.getNextFocusCar(getCars()).getIndex());
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }, delayInSeconds * 1000, intervalInSeconds * 1000);

    }

    void stopAutoFocus() {
        if (autoFocusTimer != null) {
            autoFocusTimer.cancel();
            autoFocusTimer.purge();
        }
        autoFocusser.clear();
        System.out.println("Autofocus stopped");
    }

    void focus(short carIndex) throws IOException {
        System.out.println("Focussing to " + carIndex);
        acc.focus(carIndex);
    }

    TrackInfo getTrackInfo() {
        return acc.getTrackData().map(t -> new TrackInfo(t.getTrackName(), cameraSets(t))).orElse(null);
    }

    private Map<String, List<String>> cameraSets(TrackData trackData) {
        HashMap<String, List<String>> cameraSets = new HashMap<>();
        trackData.getCameraSets().forEach(s -> cameraSets.put(s.getName(), new ArrayList<>()));
        return cameraSets;
    }

    private Car car(EntryListCar entryListCar) {
        var car = new Car();

        car.setIndex(entryListCar.getCarIndex());
        if (entryListCar.getDrivers() != null) {
            car.setDriverShortName(entryListCar.getDrivers().stream().findFirst().map(Driver::getShortName).orElse("No Name"));
            car.setDriverLongName(entryListCar.getDrivers().stream().findFirst().map(d -> String.format("%s %s", d.getFirstName(), d.getLastName())).orElse("No Name"));
        } else {
            car.setDriverShortName("No Name yet");
        }
        car.setNumber(entryListCar.getRaceNumber());

        var focusedCar = acc.getCurrentFocusedCar();
        focusedCar.filter(f -> car.getIndex() == f.getCarIndex()).ifPresent(c -> car.setHasFocus(true));

        if (acc.getCarStatsMap().containsKey(entryListCar.getCarIndex())) {
            var stats = acc.getCarStatsMap().get(entryListCar.getCarIndex());
            car.setSessionPosition(stats.getSessionPosition());
            car.setCarLocation(CarLocation.fromId(stats.getCarLocation()));
            car.setStatus(stats.getLastMessage().map(this::getBroadCastMessage).orElse(""));
        }

        return car;
    }

    private String getBroadCastMessage(BroadCastMessage message) {
        return String.format("%s %s", BroadCastMessageType.fromId(message.getType()).map(BroadCastMessageType::getLabel).orElse(""), message.getMessage());
    }

    @EventListener
    public void onDisconnected(DisconnectedEvent event) {
        stopAutoFocus();
    }

    public SessionInfo getSessionInfo() {
        return acc.getCurrentSession().map(SessionInfo::new).orElse(null);
    }
}
