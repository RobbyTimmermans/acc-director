package be.timsys.gaming.acc.director.game.model.response;

import be.timsys.gaming.acc.director.game.model.AccMessage;
import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Getter
@ToString
public class EntryList extends AccMessage {

    private int connectionId;
    private List<EntryListCar> carList;
    private short carEntryCount;

    private short driverEntryCount;
    private List<Short> driverIndexes;

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        this.connectionId = lil.readInt();
    }

    public EntryList(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
        this.carEntryCount = lil.readShort();
        this.carList = readCarList(lil);

        this.driverEntryCount = lil.readShort();
        this.driverIndexes = readDriverIndexes(lil);
    }


    private List<EntryListCar> readCarList(LittleEndianDataInputStream lil) throws IOException {
        List<EntryListCar> listCars = new ArrayList<>();
        for (short i = 0; i < carEntryCount; i++) {
            listCars.add(new EntryListCar(lil.readShort()));
        }
        return listCars;
    }

    private List<Short> readDriverIndexes(LittleEndianDataInputStream lil) throws IOException {
        List<Short> driverIndexes = new ArrayList<>();
        for (short i = 0; i < driverEntryCount; i++) {
            driverIndexes.add(lil.readShort());
        }
        return driverIndexes;
    }

}
