package be.timsys.gaming.acc.director.config;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class Config {

    private Path savedSessionsFolder;

    @PostConstruct
    private void initPath() throws IOException {
        savedSessionsFolder = Path.of(System.getProperty( "user.home" ), "acc-sessions");
        if (!Files.exists(savedSessionsFolder)) {
            Files.createDirectories(savedSessionsFolder);
        }
    }

    public Path getSavedSessionsFolder() {
        return savedSessionsFolder;
    }
}
