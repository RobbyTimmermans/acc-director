package be.timsys.gaming.acc.director.game.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class EntryListCarStats {

    private short sessionPosition;
    private byte carLocation;
    private BroadCastMessage lastMessage;
    private long totalTime;

    public Optional<BroadCastMessage> getLastMessage() {
        return Optional.ofNullable(lastMessage);
    }
}
