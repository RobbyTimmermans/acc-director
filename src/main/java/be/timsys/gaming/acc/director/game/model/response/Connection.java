package be.timsys.gaming.acc.director.game.model.response;

import be.timsys.gaming.acc.director.game.model.AccMessage;
import com.google.common.io.LittleEndianDataInputStream;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;

@Getter
@ToString
public class Connection extends AccMessage {

    private int connectionId;
    private boolean isSuccess;
    private boolean isReadOnly;
    private String error;

    public Connection(LittleEndianDataInputStream lil, byte[] raw) throws IOException {
        super(lil, raw);
    }

    @Override
    protected void build(LittleEndianDataInputStream lil) throws IOException {
        this.connectionId = lil.readInt();
        this.isSuccess = lil.readByte() > 0;
        this.isReadOnly = lil.readByte() == 0;

        this.error = readString(lil);
    }
}
