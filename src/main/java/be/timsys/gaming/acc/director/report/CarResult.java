package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.SessionType;
import lombok.Data;

import java.util.List;

@Data
public class CarResult {

    private List<String> driverNames;
    private String teamName;
    private String carNumber;
    private SessionType sessionType;
    private short position;
    private FastestLap fastestLap;
    private long totalSessionTime;

}
