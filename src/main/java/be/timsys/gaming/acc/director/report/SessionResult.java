package be.timsys.gaming.acc.director.report;

import be.timsys.gaming.acc.director.game.model.response.SessionType;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class SessionResult {

    public final SessionType sessionType;
    public final List<CarResult> carResults;
}
