package be.timsys.gaming.acc.director.session;

import be.timsys.gaming.acc.director.game.model.response.SessionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Session {
    private String name;
    private String path;
    private OffsetDateTime timeStamp;
    private String track;
    private Set<SessionType> sessionTypes;

    public void addSessionType(SessionType sessionType) {
        if (sessionTypes == null) {
            sessionTypes = new HashSet<>();
        }
        sessionTypes.add(sessionType);
    }

    public String getDate() {
        return String.format("%te/%tm/%ty", timeStamp, timeStamp, timeStamp);
    }

    public String getTime() {
        return String.format("%tR", timeStamp);
    }
}
