package be.timsys.gaming.acc.director.game.model.request;

import be.timsys.gaming.acc.director.game.model.StructWriter;

import java.io.IOException;

public class HudRequest {
    private int connectionId;
    private String hudPage;

    public HudRequest(int connectionId, String hudPage) {
        this.connectionId = connectionId;
        this.hudPage = hudPage;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(60);
        structWriter.writeByte((byte) 49);
        structWriter.writeInt(connectionId);
        structWriter.writeString(hudPage);

        return structWriter.toByteArray();
    }
}
