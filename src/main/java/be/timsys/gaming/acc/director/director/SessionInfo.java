package be.timsys.gaming.acc.director.director;

import be.timsys.gaming.acc.director.game.model.response.SessionType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SessionInfo {
    private SessionType sessionType;
}
