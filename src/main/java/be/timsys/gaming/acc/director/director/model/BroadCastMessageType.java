package be.timsys.gaming.acc.director.director.model;

import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

@Getter
public enum BroadCastMessageType {
    NONE((byte) 0, "None"), GREENFLAG((byte) 1, "GR"), SESSIONOVER((byte) 2, "SO"), PENALTYCOMMMSG((byte) 3, "P"),
    ACCIDENT((byte) 4, "IN"), LAPCOMPLETED((byte) 5, "LC"), BESTSESSIONLAP((byte) 6, "BSL"), BESTPERSONALLAP((byte) 7, "BPL");

    BroadCastMessageType(byte id, String label) {
        this.id = id;
        this.label = label;
    }

    private byte id;
    private String label;

    public static Optional<BroadCastMessageType> fromId(byte id) {
        return Stream.of(values()).filter(cl -> cl.id == id).findFirst();
    }
}
