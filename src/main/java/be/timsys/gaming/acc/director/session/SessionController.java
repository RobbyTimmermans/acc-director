package be.timsys.gaming.acc.director.session;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class SessionController {

    private final SessionManager sessionManager;

    @GetMapping("/sessions")
    public String index() {
        return "sessions-overview";
    }

    @GetMapping("/sessions/all")
    public ModelAndView getSavedSessions() throws IOException {
        var modelAndView = new ModelAndView("saved-sessions");
        modelAndView.addObject("sessions", sessionManager.getSavedSessions());
        return modelAndView;
    }

    @GetMapping("/sessions/report")
    public RedirectView reportOfSession(@RequestParam String sessionPath) throws IOException {
        // check for correct state (is there a current session open?)
        sessionManager.replaySession(sessionPath);

        return new RedirectView("/report");
    }
}
