package be.timsys.gaming.acc.director.director;

import be.timsys.gaming.acc.director.director.model.Car;
import be.timsys.gaming.acc.director.director.model.CarLocation;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
class AutoFocusser {

    private short lastListIndex = -1;
    private int nextCarNotInPitTries = 0;
    private int maxNextCarNotInPitTries = 10;

    Car getNextFocusCar(List<Car> cars) {
        if (allCarsInPitlane(cars)) {
            System.out.println("All cars in pit, just picking one");
            return cars.stream().findFirst().orElse(null);
        } else {
            return getNextCarOnTrack(cars);
        }
    }

    private boolean allCarsInPitlane(List<Car> cars) {
        return cars.stream().allMatch(c -> c.getCarLocation().equals(CarLocation.PITLANE));
    }

    private Car getNextCarOnTrack(List<Car> cars) {
        short nextListIndex = ++lastListIndex;

        if (nextListIndex >= cars.size()) {
            nextListIndex = 0;
        }
        lastListIndex = nextListIndex;
        var car = cars.get(nextListIndex);

        if (nextCarNotInPitTries < maxNextCarNotInPitTries) {
            if (CarLocation.PITLANE.equals(car.getCarLocation())) {
                System.out.println("Car is in pitlane, looking for one not in Pit " + car);
                nextCarNotInPitTries++;
                car = getNextCarOnTrack(cars);
            }
        }
        nextCarNotInPitTries = 0;
        return car;
    }

    void clear() {
        this.lastListIndex = -1;
    }
}
