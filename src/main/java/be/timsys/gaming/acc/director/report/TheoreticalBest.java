package be.timsys.gaming.acc.director.report;

import lombok.Getter;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Getter
public class TheoreticalBest {

    private final List<Integer> splits = new ArrayList<>(3);
    private long totalLapTime;

    public void addSplit(Integer splitInDuration) {
        splits.add(splitInDuration);
        this.totalLapTime = splits.stream().mapToInt(Integer::intValue).sum();
    }

    public String getFormattedLapTime() {
        return DurationFormatUtils.formatDuration(totalLapTime, "mm:ss.SSS", true);
    }

    public String getAllSplits() {
        int splitsCount = splits.size();
        return IntStream.range(0, splitsCount).mapToObj(this::getFormattedSplits).collect(Collectors.joining(" - "));
    }

    public String getFormattedSplits(int splitIndex) {
        if (splitIndex > splits.size()) {
            return "";
        }
        var durationMillis = splits.get(splitIndex);
        return DurationFormatUtils.formatDuration(durationMillis, "mm:ss.SSS", true);
    }
}
