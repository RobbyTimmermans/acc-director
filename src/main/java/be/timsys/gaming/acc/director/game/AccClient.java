package be.timsys.gaming.acc.director.game;

import be.timsys.gaming.acc.director.game.model.request.*;
import be.timsys.gaming.acc.director.game.model.response.Connection;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

@Component
public class AccClient {

    private DatagramSocket clientSocket;

    void init(InetAddress ip, int port) throws SocketException {
        System.out.println("Connecting to " + ip);
        clientSocket = new DatagramSocket(9999);
        clientSocket.connect(ip, port);
    }

    void requestConnection() throws Exception {
        System.out.println("Requesting connection");
        byte[] request = new ConnectionRequest(4, "RaceReport", "asd", "", 700).getBytes();
        clientSocket.send(new DatagramPacket(request, request.length));
    }

    void requestTrackData(Connection connection) throws IOException {
        byte[] request = new TrackDataRequest(connection.getConnectionId()).getBytes();

        clientSocket.send(new DatagramPacket(request, request.length));
    }

    void requestEntryList(Connection connection) throws IOException {
        byte[] request = new EntryListRequest(connection.getConnectionId()).getBytes();

        clientSocket.send(new DatagramPacket(request, request.length));
    }

    void shutdown(Connection connection) throws IOException {
        System.out.println("Shutting down");
        byte[] request = new ShutdownRequest(connection.getConnectionId()).getBytes();

        clientSocket.send(new DatagramPacket(request, request.length));
        clientSocket.close();

        clientSocket = null;
    }

    void requestFocus(Connection connection, short carIndex) throws IOException {
        byte[] request = new FocusRequest(connection.getConnectionId(), carIndex).getBytes();

        clientSocket.send(new DatagramPacket(request, request.length));
    }

    void requestHud(Connection connection, String hudPage) throws IOException {
        byte[] request = new HudRequest(connection.getConnectionId(), hudPage).getBytes();

        clientSocket.send(new DatagramPacket(request, request.length));
    }

    byte[] read() throws IOException {
        byte[] receiveData = new byte[1024];
        DatagramPacket receivePacket = new DatagramPacket(receiveData, 1024);
        clientSocket.receive(receivePacket);
        return receiveData;
    }

    boolean isConnected() {
        return clientSocket != null;
    }
}
