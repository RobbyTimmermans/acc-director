package be.timsys.gaming.acc.director.game.model.request;

import be.timsys.gaming.acc.director.game.model.StructWriter;

import java.io.IOException;

public class ShutdownRequest {

    private int connectionId;

    public ShutdownRequest(int connectionId) {
        this.connectionId = connectionId;
    }

    public byte[] getBytes() throws IOException {
        StructWriter structWriter = new StructWriter(5);
        structWriter.writeByte((byte) 9);
        structWriter.writeInt(connectionId);

        return structWriter.toByteArray();
    }
}
