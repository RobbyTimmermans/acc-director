package be.timsys.gaming.acc.director.report;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequiredArgsConstructor
public class ReportController {

    private final Reporter report;

    @GetMapping("/{carId}/report")
    public ModelAndView getReport(@PathVariable short carId) {
        var modelAndView = new ModelAndView("car-report");
        modelAndView.addObject("carReport", report.getCarReport(carId));
        return modelAndView;
    }

    @GetMapping("/report")
    public ModelAndView getFullReportCurrentSession() {
        var modelAndView = new ModelAndView("full-report");
        modelAndView.addObject("fullReport", report.getFullReport());
        return modelAndView;
    }
}
